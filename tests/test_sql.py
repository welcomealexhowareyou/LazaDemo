import psycopg2
import sys
import pytest

def test_inner_join_sql():
    conn = psycopg2.connect("host=localhost port=5432 dbname=postgres user=postgres password=1234")
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM orders INNER JOIN customer ON orders.customerid = customer.id;;")
    rs = cursor.fetchone()
    assert(rs == ('jeans', 3, 'Hon Qui Qzi', 1, 'Hon Qui Qzi', 'VTM', '13454321432', 3))
    print rs
    cursor.close()
    conn.close()

# Will return:

# position | customerid | customername | itemid |    name     | city |    phone    | id
# ----------+------------+--------------+--------+-------------+------+-------------+----
# jeans    |          3 | Hon Qui Qzi  |      1 | Hon Qui Qzi | VTM  | 13454321432 |  3
# trousers |          2 | Misha        |      2 | Misha       | MOW  | 79162291432 |  2
# john     |          1 | john         |      1 | john        | BGK  | 49102300403 |  1
# (3 rows)

def test_left_outer_join_sql():
    conn = psycopg2.connect("host=localhost port=5432 dbname=postgres user=postgres password=1234")
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM wear LEFT OUTER JOIN orders ON wear.id = orders.itemid;")
    rs = cursor.fetchone()
    assert(rs == ('jeans', 100, 20, 'good jeans', 1, 'jeans', 3, 'Hon Qui Qzi', 1))
    print rs
    cursor.close()
    conn.close()

#Will return:

# item   | price | qty |                          descr                          | id | position | customerid | customername | itemid
# ----------+-------+-----+---------------------------------------------------------+----+----------+------------+--------------+--------
# jeans    |   100 |  20 | good jeans                                              |  1 | jeans    |          3 | Hon Qui Qzi  |      1
# trousers |   250 |   5 | elite D&G trousers                                      |  2 | trousers |          2 | Misha        |      2
# jeans    |   100 |  20 | good jeans                                              |  1 | john     |          1 | john         |      1
# raincoat |   193 |   2 | ultimate extreme premium ultra raincoat special for you |  3 |          |            |              |
# (4 rows)

def test_cross_join_sql():
    conn = psycopg2.connect("host=localhost port=5432 dbname=postgres user=postgres password=1234")
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM wear, orders;")
    rs = cursor.fetchone()
    assert(rs == ('jeans', 100, 20, 'good jeans', 1, 'jeans', 3, 'Hon Qui Qzi', 1))
    print rs
    cursor.close()
    conn.close()



#     postgres=# SELECT * FROM wear, orders;
# item   | price | qty |                          descr                          | id | position | customerid | customername | itemid
# ----------+-------+-----+---------------------------------------------------------+----+----------+------------+--------------+--------
# jeans    |   100 |  20 | good jeans                                              |  1 | jeans    |          3 | Hon Qui Qzi  |      1
# trousers |   250 |   5 | elite D&G trousers                                      |  2 | jeans    |          3 | Hon Qui Qzi  |      1
# raincoat |   193 |   2 | ultimate extreme premium ultra raincoat special for you |  3 | jeans    |          3 | Hon Qui Qzi  |      1
# jeans    |   100 |  20 | good jeans                                              |  1 | trousers |          2 | Misha        |      2
# trousers |   250 |   5 | elite D&G trousers                                      |  2 | trousers |          2 | Misha        |      2
# raincoat |   193 |   2 | ultimate extreme premium ultra raincoat special for you |  3 | trousers |          2 | Misha        |      2
#     jeans    |   100 |  20 | good jeans                                              |  1 | john     |          1 | john         |      1
# trousers |   250 |   5 | elite D&G trousers                                      |  2 | john     |          1 | john         |      1
# raincoat |   193 |   2 | ultimate extreme premium ultra raincoat special for you |  3 | john     |          1 | john         |      1
# (9 rows)